
PACKAGE_TARGETS := ansible ansible.cfg assemblies assembly-runner.py \
	Makefile rabbit_vars.yml README.md requirements.txt utils

CTAGS := $(shell command -v ctags 2> /dev/null)
CTAGS-exists : ; @which ctags > /dev/null
PIP-exists := $(shell command -v pip3 2> /dev/null)

check_pip3:
ifndef PIP-exists
	$(error "pip3 is not available - please install it.")
endif

install_deps: check_pip3 requirements.txt
	pip3 install -r requirements.txt

remove_deps:
	-rm -rf venv

tags: CTAGS-exists
	ctags -R --fields=+l --languages=python --python-kinds=-iv \
	    --exclude=backups --exclude=ansible --exclude=expes

clean:
	-rm -rf results*.json

