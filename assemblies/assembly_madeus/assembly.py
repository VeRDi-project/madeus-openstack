
from concerto.madeus.all import MadeusAssembly

from assemblies.assembly_madeus.facts import Facts
from assemblies.assembly_madeus.memcached import MemCached
from assemblies.assembly_madeus.common import Common
from assemblies.assembly_madeus.rabbitmq import RabbitMQ
from assemblies.assembly_madeus.openvswitch import OpenVSwitch
from assemblies.assembly_madeus.haproxy import HAProxy
from assemblies.assembly_madeus.mariadb import MariaDB
from assemblies.assembly_madeus.keystone import Keystone
from assemblies.assembly_madeus.glance import Glance
from assemblies.assembly_madeus.neutron import Neutron
from assemblies.assembly_madeus.nova import Nova


class MMadeusAssembly(MadeusAssembly):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook_dir = playbook_dir
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusAssembly.__init__(self)

    def create(self):
        self.components = {
            "facts": Facts(self.playbook_dir, self.inventory, self.vars_dir),
            "common": Common(self.playbook_dir, self.inventory, self.vars_dir),
            "haproxy": HAProxy(self.playbook_dir, self.inventory, self.vars_dir),
            "memcached": MemCached(self.playbook_dir, self.inventory, self.vars_dir),
            "openvswitch": OpenVSwitch(self.playbook_dir, self.inventory, self.vars_dir),
            "rabbitmq": RabbitMQ(self.playbook_dir, self.inventory, self.vars_dir),
            "mariadb": MariaDB(self.playbook_dir, self.inventory, self.vars_dir),
            "keystone": Keystone(self.playbook_dir, self.inventory, self.vars_dir),
            "glance": Glance(self.playbook_dir, self.inventory, self.vars_dir),
            "neutron": Neutron(self.playbook_dir, self.inventory, self.vars_dir),
            "nova": Nova(self.playbook_dir, self.inventory, self.vars_dir)
        }

        self.dependencies = [
            ("facts", "facts", "common", "facts"),
            ("facts", "facts", "haproxy", "facts"),
            ("facts", "facts", "memcached", "facts"),
            ("facts", "facts", "rabbitmq", "facts"),
            ("facts", "facts", "openvswitch", "facts"),
            ("common", "common", "mariadb", "common"),
            ("haproxy", "haproxy", "mariadb", "haproxy"),
            ("mariadb", "mariadb", "keystone", "mariadb"),
            ("mariadb", "mariadb", "glance", "mariadb"),
            ("mariadb", "mariadb", "neutron", "mariadb"),
            ("mariadb", "mariadb", "nova", "mariadb"),
            ("keystone", "keystone", "glance", "keystone"),
            ("keystone", "keystone", "neutron", "keystone"),
            ("keystone", "keystone", "nova", "keystone")
        ]

