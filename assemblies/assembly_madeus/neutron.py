
import logging
from pathlib import Path

from concerto.madeus.all import (MadeusComponent, DepType)

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip
from utils.constants import (ANSIBLE_DIR, SYMLINK_NAME)



class Neutron(MadeusComponent):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook = str(Path(playbook_dir) / "site_neutron.yml")
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusComponent.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'deployed'
        ]

        self.initial_place = "initiated"

        self.transitions = {
            'pull': ('initiated', 'pulled', self.pull),
            'config': ('initiated', 'pulled', self.config),
            'register': ('initiated', 'pulled', self.register),
            'deploy': ('pulled', 'deployed', self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['config']),
            'keystone': (DepType.USE, ['register']),
            'neutron': (DepType.PROVIDE, ['deployed'])
        }

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling Neutron start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=pull"],
                self.vars_dir)

    def config(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Configuring Neutron start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_config"],
                self.vars_dir)

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering Neutron start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_register"],
                self.vars_dir)

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying Neutron start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_deploy"],
                self.vars_dir)

