
import logging
from pathlib import Path

from concerto.madeus.all import (MadeusComponent, DepType)

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip



class OpenVSwitch(MadeusComponent):
    """ Defines a component for the OpenVSwitch deployment """

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook = str(Path(playbook_dir) / "site_openvswitch.yml")
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusComponent.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'deployed'
        ]

        self.initial_place = 'initiated'

        self.transitions = {
            'deploy': ('initiated', 'deployed', self.deploy)
        }

        self.dependencies = {
            'facts': (DepType.USE, ['deploy']),
            'openvswitch': (DepType.PROVIDE, ['deployed'])
        }

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying OpenVSwitch start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=deploy"],
                self.vars_dir)

