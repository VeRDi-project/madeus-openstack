
import logging
from pathlib import Path

from concerto.madeus.all import (MadeusComponent, DepType)

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip



class Nova(MadeusComponent):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook = str(Path(playbook_dir) / "site_nova.yml")
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusComponent.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'ready',
            'restarted',
            'deployed'
        ]

        self.initial_place = 'initiated'

        self.transitions = {
            'register': ('initiated', 'deployed', self.register),
            'pull': ('initiated', 'pulled', self.pull),
            'config': ('initiated', 'pulled', self.config),
            'create_db': ('initiated', 'pulled', self.create_db),
            'upgrade_db': ('pulled', 'ready', self.upgrade_db),
            'upgrade_api_db': ('pulled', 'ready', self.upgrade_api_db),
            'restart': ('ready', 'restarted', self.restart),
            'cell_setup': ('restarted', 'deployed', self.cell_setup),
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['create_db']),
            'keystone': (DepType.USE, ['register']),
            'nova': (DepType.PROVIDE, ['deployed'])
        }

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=pull"],
                self.vars_dir)

    def config(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Configuring Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_config"],
                self.vars_dir)

    def create_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Creating Database Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_create_db"],
                self.vars_dir)

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_register"],
                self.vars_dir)

    def upgrade_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Upgrading database Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_upgrade_db"],
                self.vars_dir)

    def upgrade_api_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Upgrading API db Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_upgrade_api_db"],
                self.vars_dir)

    def restart(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Restarting Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_restart"],
                self.vars_dir)

    def cell_setup(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_simple_cell_setup"],
                self.vars_dir)

