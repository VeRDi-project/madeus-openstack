
import logging
from pathlib import Path

from concerto.madeus.all import (MadeusComponent, DepType)

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip



class MariaDB(MadeusComponent):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook = str(Path(playbook_dir) / "site_mariadb.yml")
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusComponent.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'configured',
            'bootstrapped',
            'restarted',
            'registered',
            'deployed'
        ]

        self.initial_place = 'initiated'

        self.transitions = {
            'pull': ('initiated', 'configured', self.pull),
            'config': ('initiated', 'configured', self.config),
            'bootstrap': ('configured', 'bootstrapped', self.bootstrap),
            'restart': ('bootstrapped', 'restarted', self.restart),
            'register': ('restarted', 'registered', self.register),
            'check': ('registered', 'deployed', self.check)
        }

        self.dependencies = {
            'common': (DepType.USE, ['register']),
            'haproxy': (DepType.USE, ['restart']),
            'mariadb': (DepType.PROVIDE, ['deployed'])
        }

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=pull"],
                self.vars_dir)

    def config(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Configurating mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_config"],
                self.vars_dir)

    def bootstrap(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Bootstrapping mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_bootstrap"],
                self.vars_dir)

    def restart(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Restarting mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_restart"],
                self.vars_dir)

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_register"],
                self.vars_dir)

    def check(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Checking mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_check"],
                self.vars_dir)

