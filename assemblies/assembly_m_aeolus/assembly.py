
from concerto.madeus.all import MadeusAssembly

from assemblies.assembly_m_aeolus.facts import Facts
from assemblies.assembly_m_aeolus.memcached import MemCached
from assemblies.assembly_m_aeolus.common import Common
from assemblies.assembly_m_aeolus.rabbitmq import RabbitMQ
from assemblies.assembly_m_aeolus.openvswitch import OpenVSwitch
from assemblies.assembly_m_aeolus.haproxy import HAProxy
from assemblies.assembly_m_aeolus.mariadb import MariaDB
from assemblies.assembly_m_aeolus.keystone import Keystone
from assemblies.assembly_m_aeolus.glance import Glance
from assemblies.assembly_m_aeolus.neutron import Neutron
from assemblies.assembly_m_aeolus.nova import Nova


class MAeolusAssembly(MadeusAssembly):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook_dir = playbook_dir
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusAssembly.__init__(self)

    def create(self):
        self.components = {
            "facts": Facts(self.playbook_dir, self.inventory, self.vars_dir),
            "common": Common(self.playbook_dir, self.inventory, self.vars_dir),
            "haproxy": HAProxy(self.playbook_dir, self.inventory, self.vars_dir),
            "memcached": MemCached(self.playbook_dir, self.inventory, self.vars_dir),
            "mariadb": MariaDB(self.playbook_dir, self.inventory, self.vars_dir),
            "rabbitmq": RabbitMQ(self.playbook_dir, self.inventory, self.vars_dir),
            "keystone": Keystone(self.playbook_dir, self.inventory, self.vars_dir),
            "openvswitch": OpenVSwitch(self.playbook_dir, self.inventory, self.vars_dir),
            "glance": Glance(self.playbook_dir, self.inventory, self.vars_dir),
            "neutron": Neutron(self.playbook_dir, self.inventory, self.vars_dir),
            "nova": Nova(self.playbook_dir, self.inventory, self.vars_dir)
        }

        self.dependencies = [
            ("facts", "facts", "haproxy", "facts"),
            ("facts", "facts", "openvswitch", "facts"),
            ("facts", "facts", "memcached", "facts"),
            ("facts", "facts", "rabbitmq", "facts"),
            ("facts", "facts", "common", "facts"),
            ("common", "common", "mariadb", "common"),
            ("haproxy", "haproxy", "mariadb", "haproxy"),
            ("mariadb", "mariadb", "keystone", "mariadb"),
            ("keystone", "keystone", "glance", "keystone"),
            ("keystone", "keystone", "nova", "keystone"),
            ("keystone", "keystone", "neutron", "keystone")
        ]

