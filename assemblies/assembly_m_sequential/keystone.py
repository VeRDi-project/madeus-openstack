
import logging
from pathlib import Path

from concerto.madeus.all import (MadeusComponent, DepType)

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip



class Keystone(MadeusComponent):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook = str(Path(playbook_dir) / "site_keystone.yml")
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusComponent.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'deployed'
        ]

        self.initial_place = 'initiated'

        self.transitions = {
            'pull': ('initiated', 'pulled', self.pull),
            'deploy': ('pulled', 'deployed', self.deploy)
        }

        self.dependencies = {
            'rabbitmq': (DepType.USE, ['pull']),
            'keystone': (DepType.PROVIDE, ['deployed'])
        }
        
    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling keystone start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=pull"],
                self.vars_dir)

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying keystone start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=deploy"],
                self.vars_dir)

