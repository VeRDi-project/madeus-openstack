
from concerto.madeus.all import MadeusAssembly

from assemblies.assembly_m_sequential.facts import Facts
from assemblies.assembly_m_sequential.memcached import MemCached
from assemblies.assembly_m_sequential.common import Common
from assemblies.assembly_m_sequential.rabbitmq import RabbitMQ
from assemblies.assembly_m_sequential.openvswitch import OpenVSwitch
from assemblies.assembly_m_sequential.haproxy import HAProxy
from assemblies.assembly_m_sequential.mariadb import MariaDB
from assemblies.assembly_m_sequential.keystone import Keystone
from assemblies.assembly_m_sequential.glance import Glance
from assemblies.assembly_m_sequential.neutron import Neutron
from assemblies.assembly_m_sequential.nova import Nova


class MSequentialAssembly(MadeusAssembly):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook_dir = playbook_dir
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusAssembly.__init__(self)

    def create(self):
        self.components = {
            "facts": Facts(self.playbook_dir, self.inventory, self.vars_dir),
            "common": Common(self.playbook_dir, self.inventory, self.vars_dir),
            "haproxy": HAProxy(self.playbook_dir, self.inventory, self.vars_dir),
            "memcached": MemCached(self.playbook_dir, self.inventory, self.vars_dir),
            "mariadb": MariaDB(self.playbook_dir, self.inventory, self.vars_dir),
            "rabbitmq": RabbitMQ(self.playbook_dir, self.inventory, self.vars_dir),
            "keystone": Keystone(self.playbook_dir, self.inventory, self.vars_dir),
            "openvswitch": OpenVSwitch(self.playbook_dir, self.inventory, self.vars_dir),
            "glance": Glance(self.playbook_dir, self.inventory, self.vars_dir),
            "neutron": Neutron(self.playbook_dir, self.inventory, self.vars_dir),
            "nova": Nova(self.playbook_dir, self.inventory, self.vars_dir)
        }

        self.dependencies = [
            ("facts", "facts", "common", "facts"),
            ("common", "common", "haproxy", "common"),
            ("haproxy", "haproxy", "memcached", "haproxy"),
            ("memcached", "memcached", "mariadb", "memcached"),
            ("mariadb", "mariadb", "rabbitmq", "mariadb"),
            ("rabbitmq", "rabbitmq", "keystone", "rabbitmq"),
            ("keystone", "keystone", "glance", "keystone"),
            ("glance", "glance", "nova", "glance"),
            ("nova", "nova", "openvswitch", "nova"),
            ("openvswitch", "openvswitch", "neutron", "openvswitch")
        ]

