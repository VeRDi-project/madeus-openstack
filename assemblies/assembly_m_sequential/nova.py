
import logging
from pathlib import Path

from concerto.madeus.all import (MadeusComponent, DepType)

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip



class Nova(MadeusComponent):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook = str(Path(playbook_dir) / "site_nova.yml")
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusComponent.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'configured',
            'registered',
            'created',
            'upgraded',
            'upgraded_api',
            'restarted',
            'deployed'
        ]

        self.initial_place = 'initiated'

        self.transitions = {
            'pull': ('initiated', 'pulled', self.pull),
            'config': ('pulled', 'configured', self.config),
            'register': ('configured', 'registered', self.register),
            'create_db': ('registered', 'created', self.create_db),
            'upgrade_db': ('created', 'upgraded', self.upgrade_db),
            'upgrade_api_db': ('upgraded', 'upgraded_api', self.upgrade_api_db),
            'restart': ('upgraded_api', 'restarted', self.restart),
            'cell_setup': ('restarted', 'deployed', self.cell_setup),
        }

        self.dependencies = {
            'glance': (DepType.USE, ['pull']),
            'nova': (DepType.PROVIDE, ['deployed'])
        }

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=pull"],
                self.vars_dir)

    def config(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Configuring Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_config"],
                self.vars_dir)

    def create_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Creating Database Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_create_db"],
                self.vars_dir)

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_register"],
                self.vars_dir)

    def upgrade_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Upgrading database Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_upgrade_db"],
                self.vars_dir)

    def upgrade_api_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Upgrading API db Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_upgrade_api_db"],
                self.vars_dir)

    def restart(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Restarting Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_restart"],
                self.vars_dir)

    def cell_setup(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_simple_cell_setup"],
                self.vars_dir)

