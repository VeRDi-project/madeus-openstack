
import logging
from pathlib import Path

from concerto.madeus.all import (MadeusComponent, DepType)

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip



class Common(MadeusComponent):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook = str(Path(playbook_dir) / "site_common.yml")
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusComponent.__init__(self)

    def create(self):

        self.places = [
            'initiated',
            'deployed',
            'ktb_deployed'
        ]

        self.initial_place = 'initiated'

        self.transitions = {
            'ktb_deploy': ('initiated', 'ktb_deployed', self.ktb_deploy),
            'deploy': ('ktb_deployed', 'deployed', self.deploy)
        }

        self.dependencies = {
            'common': (DepType.PROVIDE, ['deployed']),
            'facts': (DepType.USE, ['ktb_deploy'])
        }

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : oth deployment of common start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=oth_deploy"],
                self.vars_dir)

    def ktb_deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : ktb deployment of common start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=ktb_deploy"],
                self.vars_dir)

