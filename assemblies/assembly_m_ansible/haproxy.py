
import logging
from pathlib import Path

from concerto.madeus.all import (MadeusComponent, DepType)

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip



class HAProxy(MadeusComponent):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook = str(Path(playbook_dir) / "site_haproxy.yml")
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusComponent.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'deployed'
        ]

        self.initial_place = 'initiated'

        self.transitions = {
            'deploy': ('initiated', 'deployed', self.deploy)
        }

        self.dependencies = {
            'haproxy': (DepType.PROVIDE, ['deployed']),
            'common': (DepType.USE, ['deploy'])
        }

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying HAProxy start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=deploy"],
                self.vars_dir)

