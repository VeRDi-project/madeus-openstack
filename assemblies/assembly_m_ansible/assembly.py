
from concerto.madeus.all import MadeusAssembly

from assemblies.assembly_m_ansible.facts import Facts
from assemblies.assembly_m_ansible.memcached import MemCached
from assemblies.assembly_m_ansible.common import Common
from assemblies.assembly_m_ansible.rabbitmq import RabbitMQ
from assemblies.assembly_m_ansible.openvswitch import OpenVSwitch
from assemblies.assembly_m_ansible.haproxy import HAProxy
from assemblies.assembly_m_ansible.mariadb import MariaDB
from assemblies.assembly_m_ansible.keystone import Keystone
from assemblies.assembly_m_ansible.glance import Glance
from assemblies.assembly_m_ansible.neutron import Neutron
from assemblies.assembly_m_ansible.nova import Nova


class MAnsibleAssembly(MadeusAssembly):

    def __init__(self, playbook_dir, inventory, vars_dir):
        self.playbook_dir = playbook_dir
        self.inventory = inventory
        self.vars_dir = vars_dir

        MadeusAssembly.__init__(self)

    def create(self):
        self.components = {
            "facts": Facts(self.playbook_dir, self.inventory, self.vars_dir),
            "common": Common(self.playbook_dir, self.inventory, self.vars_dir),
            "haproxy": HAProxy(self.playbook_dir, self.inventory, self.vars_dir),
            "memcached": MemCached(self.playbook_dir, self.inventory, self.vars_dir),
            "mariadb": MariaDB(self.playbook_dir, self.inventory, self.vars_dir),
            "rabbitmq": RabbitMQ(self.playbook_dir, self.inventory, self.vars_dir),
            "keystone": Keystone(self.playbook_dir, self.inventory, self.vars_dir),
            "openvswitch": OpenVSwitch(self.playbook_dir, self.inventory, self.vars_dir),
            "glance": Glance(self.playbook_dir, self.inventory, self.vars_dir),
            "neutron": Neutron(self.playbook_dir, self.inventory, self.vars_dir),
            "nova": Nova(self.playbook_dir, self.inventory, self.vars_dir)
        }

        self.dependencies = [
            ("facts", "facts", "common", "facts"),
            ("common", "common", "haproxy", "common"),
            ("haproxy", "haproxy", "memcached", "haproxy"),
            ("memcached", "memcached", "mariadb", "memcached"),
            ("mariadb", "mariadb", "rabbitmq", "mariadb"),
            ("rabbitmq", "rabbitmq", "keystone", "rabbitmq"),
            ("keystone", "keystone", "glance", "keystone"),
            ("glance", "glance", "nova", "glance"),
            ("nova", "nova", "openvswitch", "nova"),
            ("openvswitch", "openvswitch", "neutron", "openvswitch")
        ]

